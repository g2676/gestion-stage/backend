from django.apps import AppConfig


class GestionStageConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gestion_stage'
